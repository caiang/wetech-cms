<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<div class="am-g am-g-fixed am-u-sm-centered">
	<!-- banner start -->
	<jsp:include page="/jsp/template/banner.jsp" />
	<!-- banner end -->
	<div class="am-u-md-4 am-u-sm-12">
		<div data-am-widget="list_news" class="am-list-news am-list-news-default">
			<!--列表标题-->
            <div class="am-list-news-hd am-cf">
                <!--带更多链接-->
                <h2><span class="am-icon-send-o"></span>&nbsp;&nbsp;最近更新</h2>
            </div>
			<div class="am-list-news-bd">
				<ul class="am-list">
					<li class="am-g am-list-item-dated"><a href="topic/97" class="am-list-item-hd ">宝天曼、内乡县衙、峡谷漂流路线推荐</a> <span class="am-list-date">2019-01-20</span></li>
					<li class="am-g am-list-item-dated"><a href="topic/96" class="am-list-item-hd ">宝天曼美食之城关缸炉烧饼</a> <span class="am-list-date">2019-01-20</span></li>
					<li class="am-g am-list-item-dated"><a href="topic/95" class="am-list-item-hd ">宝天曼美食之马山口油条</a> <span class="am-list-date">2019-01-20</span></li>
					<li class="am-g am-list-item-dated"><a href="topic/94" class="am-list-item-hd ">宝天曼美食之内乡卷煎</a> <span class="am-list-date">2019-01-20</span></li>
					<li class="am-g am-list-item-dated"><a href="topic/93" class="am-list-item-hd ">宝天曼美食之内乡酸菜</a> <span class="am-list-date">2019-01-20</span></li>
					<li class="am-g am-list-item-dated"><a href="topic/92" class="am-list-item-hd ">内乡宝天曼</a> <span class="am-list-date">2019-01-20</span></li>
				</ul>
			</div>
		</div>
	</div>
</div>
<!-- channel start -->
<div class="am-g am-g-fixed  cms-fixed">
	<div class="am-u-md-6 am-u-sm-12">
		<div data-am-widget="titlebar" class="am-titlebar am-titlebar-default">
			<h2 class="am-titlebar-title ">景点大全</h2>
			<nav class="am-titlebar-nav">
				<a href="channel/57" class="">more &raquo;</a>
			</nav>
		</div>
		<div data-am-widget="list_news" class="am-list-news am-list-news-default">
			<!--列表标题-->
			<ul class="am-list">
				<li class="am-g am-list-item-desced am-list-item-thumbed am-list-item-thumb-left">
					<div class="am-u-sm-3 am-list-thumb">
						<a href="topic/92"> <img src="http://www.baotianman.cn/uploads/2016/1014/20161014110405198.jpg" alt="内乡宝天曼" />
						</a>
					</div>
					<div class=" am-u-sm-9 am-list-main">
						<a class="am-channel-title" href="topic/92">内乡宝天曼</a> 
						<span class="am-channel-date">2019-01-20</span>
						<div class="am-list-item-text">内向宝天曼旅游生态文化区</div>
					</div>
				</li>
				<li class="am-g am-list-item-desced am-list-item-thumbed am-list-item-thumb-left">
					<div class="am-u-sm-3 am-list-thumb">
						<a href="topic/90"> <img src="https://gss3.bdstatic.com/-Po3dSag_xI4khGkpoWK1HF6hhy/baike/c0%3Dbaike72%2C5%2C5%2C72%2C24/sign=bd968173c91b9d169eca923392b7dfea/024f78f0f736afc3b2612cc9b419ebc4b7451228.jpg" alt="伏牛山" />
						</a>
					</div>
					<div class=" am-u-sm-9 am-list-main">
						<a class="am-channel-title" href="topic/90">伏牛山</a> 
						<span class="am-channel-date">2019-01-20</span>
						<div class="am-list-item-text">伏牛山简介</div>
					</div>
				</li>
				<li class="am-g am-list-item-desced am-list-item-thumbed am-list-item-thumb-left">
					<div class="am-u-sm-3 am-list-thumb">
						<a href="topic/91"> <img src="https://gss0.bdstatic.com/-4o3dSag_xI4khGkpoWK1HF6hhy/baike/c0%3Dbaike116%2C5%2C5%2C116%2C38/sign=cef7a0a9ca95d143ce7bec711299e967/4b90f603738da977b542ca81b251f8198618e32f.jpg" alt="老界岭" />
						</a>
					</div>
					<div class=" am-u-sm-9 am-list-main">
						<a class="am-channel-title" href="topic/91">老界岭</a> 
						<span class="am-channel-date">2019-01-20</span>
						<div class="am-list-item-text">老界岭简介</div>
					</div>
				</li>
			</ul>
		</div>
	</div>
<!-- channel end -->
<!-- last topic start -->
<div class="am-g am-g-fixed cms-fixed">
    <div class="am-u-md-8">
        <div data-am-widget="list_news" class="am-list-news am-list-news-default am-no-layout">

            <div class="am-list-news-hd am-cf">

                <h2>
                    <span class="am-icon-list-ul"></span> 文章列表
                </h2>
            </div>
            <div class="am-list-news-bd">

                <ul class="am-list" id="topicList"></ul>
                <div style="display:none;" class="am-g am-text-middle" id="loading">
                    <button class="am-btn am-btn-xs am-btn-block"> <span class="am-icon-spinner am-icon-spin"></span>&nbsp;&nbsp;加载中&nbsp;&nbsp;</button>
                </div>
            </div>

        </div>
    </div>
    <div class="am-u-md-4">

        <div class="am-panel-group am-hide-sm-only">
            <section class="am-panel am-panel-default">
                <div class="am-panel-hd">推荐阅读</div>
                <ul class="am-list blog-list">
                </ul>
            </section>
            <section class="am-panel am-panel-default">
                <div class="am-panel-hd">标签云</div>
                <div class="am-panel-bd">
                    <a href="keyword/南阳" class="am-badge am-badge-primary am-text-sm am-radius am-badge-warning">南阳</a>
                    <a href="keyword/美食" class="am-badge am-badge-primary am-text-sm am-radius am-badge">美食</a>
                    <a href="keyword/五星级" class="am-badge am-badge-primary am-text-sm am-radius am-badge-danger">五星级</a>
                    <a href="keyword/自然" class="am-badge am-badge-primary am-text-sm am-radius am-badge-success">自然</a>
                    <a href="keyword/油条" class="am-badge am-badge-primary am-text-sm am-radius am-badge-primary">油条</a>
                    <a href="keyword/历史" class="am-badge am-badge-primary am-text-sm am-radius am-badge">历史</a>
                    <a href="keyword/内向" class="am-badge am-badge-primary am-text-sm am-radius am-badge-secondary">内向</a>
                    <a href="keyword/武侯祠" class="am-badge am-badge-primary am-text-sm am-radius am-badge-primary">武侯祠</a>
                    <a href="keyword/老界岭" class="am-badge am-badge-primary am-text-sm am-radius am-badge">老界岭</a>
                    <a href="keyword/伏牛山" class="am-badge am-badge-primary am-text-sm am-radius am-badge-primary">伏牛山</a>
                    <a href="keyword/宝天曼" class="am-badge am-badge-primary am-text-sm am-radius am-badge-success">宝天曼</a>
                    <a href="keyword/酸菜" class="am-badge am-badge-primary am-text-sm am-radius am-badge">酸菜</a>
                    <a href="keyword/烧饼" class="am-badge am-badge-primary am-text-sm am-radius am-badge">烧饼</a>
				</div>
            </section>
        </div>
    </div>
</div>