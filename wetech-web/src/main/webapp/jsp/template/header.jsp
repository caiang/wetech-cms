<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<header class="am-topbar am-topbar-fixed-top am-topbar-inverse">
	<div class="am-container">
		<h1 class="am-topbar-brand">
			<a href="<%=request.getContextPath()%>/index">WETECH CMS</a>
		</h1>
		<button class="am-topbar-btn am-topbar-toggle am-btn am-btn-sm am-btn-primary am-show-sm-only" data-am-collapse="{target: '#collapse-head'}">
			<span class="am-sr-only">导航切换</span><span class="am-icon-bars"></span>
		</button>
		<div class="am-collapse am-topbar-collapse" id="collapse-head">
			<ul class="am-nav am-nav-pills am-topbar-nav">
				<li class=""><a href="<%=request.getContextPath()%>/index">首页</a></li>
				<li><a href="<%=request.getContextPath()%>/channel/56">走进南阳</a></li>
				<li><a href="<%=request.getContextPath()%>/channel/57">景点大全</a></li>
				<li class="am-dropdown" data-am-dropdown><a class="am-dropdown-toggle" data-am-dropdown-toggle href="javascript:;"> 旅游指南 <span class="am-icon-caret-down"></span></a>
					<ul class="am-dropdown-content">
						<li><a href="<%=request.getContextPath()%>/channel/59">特色美食</a></li>
						<li><a href="<%=request.getContextPath()%>/channel/60">经典路线</a></li>
						<li><a href="<%=request.getContextPath()%>/channel/61">地方特产</a></li>
						<li><a href="<%=request.getContextPath()%>/channel/62">休闲娱乐</a></li>
						<li><a href="<%=request.getContextPath()%>/channel/63">交通路线</a></li>
					</ul>
				</li>
			</ul>
			<form class="am-topbar-form am-topbar-left  am-form-inline" role="search">
				<div class="am-form-group am-form-icon">
					<div class="am-dropdown" id='dropdown-search'>
						<i class="am-icon-search"></i> <input type="text" id="dropdown-search-input" class="am-form-field am-input-sm" placeholder=" 搜索你感兴趣的内容...">
						<ul class="am-dropdown-content am-dropdown-search" id="dropdown-search-ul"></ul>
					</div>
				</div>
			</form>
			<div class="am-topbar-right">
				<c:if test="${loginUser!=null}">
					<a href="<%=request.getContextPath()%>/admin"><button class="am-btn am-btn-primary am-topbar-btn am-btn-sm"><span class="am-icon-user"></span> ${loginUser.nickname }</button></a>
				</c:if>
				<c:if test="${loginUser==null}">
					<a href="<%=request.getContextPath()%>/login"><button class="am-btn am-btn-primary am-topbar-btn am-btn-sm"><span class="am-icon-user"></span> 登录</button></a>
				</c:if>
			</div>
		</div>
	</div>
	<script>
		var contextPath = "${pageContext.request.contextPath}";
    </script>
</header>